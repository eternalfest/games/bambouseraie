package quete;

import patchman.IPatch;
import patchman.Ref;
import merlin.IAction;
import etwin.ds.FrozenArray;
import hf.Hf;
import etwin.Obfu;
import patchman.HostModuleLoader;

@:build(patchman.Build.di())
class Quetes {
  
	public var hml 				: HostModuleLoader;
	public var data				: patchman.module.Data;
	public  var donneesQuetes 	: Array<Dynamic>;
	
	@:diExport
	public var quetes(default, null): IPatch;
	
	@:diExport
	public var trigger_quest(default, null): IAction = new quete.actions.Quest();

	public function new(hml: HostModuleLoader,d : patchman.module.Data) {
		this.data = d;
		var donneesQuetes: Array<Dynamic> = d.get(Obfu.raw("quetes"));
		this.quetes = Ref
		.auto(hf.mode.GameMode.initWorld)
		.after(function(hf,self): Void {
			var items = Quetes.getItemsFromHml(hml);
			for (quete in donneesQuetes){
				// ID de la quête
				var id			: String = Reflect.field(quete, Obfu.raw("id"));
				// Objets nécessaire à sa validation
				var require		: Dynamic = Reflect.field(quete, Obfu.raw("require"));
				// Familles débloquées : >= 5000 = clef
				var give		: Null<Array<Int>>= Reflect.field(quete, Obfu.raw("give"));
				// Familles bloquées : >= 5000 = clef
				var remove		: Null<Array<Int>> = Reflect.field(quete, Obfu.raw("remove"));
				
				if (Quetes.estQueteCompleteeViaItems(require, items)){
					patchman.DebugConsole.log('La quête (' + Std.string(id) +') est débloquée.');
					for (famille in give){
						Quetes.debloqueFamille(self, famille);
					}
					for (famille in remove){
						Quetes.bloqueFamille(self, famille);
					}
				}else{
					patchman.DebugConsole.log('La quête (' + Std.string(id) +') n\'est pas débloquée.');
				}
			}
		});
	}

	public static function debloqueFamille(self : hf.mode.GameMode, n : Int) : Void {
		if( n >= 1000 && n < 2000){
			var config = self.root.GameManager.CONFIG.scoreItemFamilies;
				config.push(n);
			self.randMan.register(self.root.Data.RAND_SCORES_ID, self.root.Data.getRandFromFamilies(self.root.Data.SCORE_ITEM_FAMILIES, config));
		}else if (n < 1000) {
			var config = self.root.GameManager.CONFIG.specialItemFamilies;
				config.push(n);
			self.randMan.register(self.root.Data.RAND_ITEMS_ID, self.root.Data.getRandFromFamilies(self.root.Data.SPECIAL_ITEM_FAMILIES, config));
		}else if (n >= 5000){
			self.giveKey(n - 5000);
		}
	}
	
	public static function bloqueFamille(self : hf.mode.GameMode, n : Int) : Void {
		if( n >= 1000 && n < 2000){
			self.randMan.remove(self.root.Data.RAND_SCORES_ID, n);
		}else if (n < 1000) {
			self.randMan.remove(self.root.Data.RAND_ITEMS_ID, n);
		}else if (n >= 5000){
			self.worldKeys[n - 5000] = false;
		}
	}

	public static function getItemsFromHml(hml: HostModuleLoader): Dynamic {
		var runStartMod: Dynamic = hml.require("run-start");
		var runStartData: Null<Dynamic> = Reflect.callMethod(runStartMod, Reflect.field(runStartMod, Obfu.raw("get")), []);
		if (runStartData == null) {
			patchman.DebugConsole.error("Erreur: la partie n'a pas encore été lancée !");
			return null;
		}
		var items: Dynamic = Reflect.field(runStartData, Obfu.raw("items"));
		return items;
	}
	
	public static function estQueteCompleteeViaItems(necessaire : Dynamic, items : Dynamic) : Bool{
		for (clef in Reflect.fields(necessaire)){
			var n : Int = Reflect.field(necessaire, clef);
			var possession : Int = items[Std.parseInt(clef)];
			if (possession == null)
				possession = 0;
			if (possession < n){
				return false;
			}
		}
		return true;
	}
}