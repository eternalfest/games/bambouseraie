package quete.actions;

import merlin.IAction;
import merlin.IActionContext;
import patchman.HostModuleLoader;
import etwin.Obfu;

import hf.GameManager;
import quete.Quetes;

class Quest implements IAction {

	public var name(default, null): String = Obfu.raw("quest");
	public var isVerbose(default, null): Bool = true;
    
    public function new() {
    }

	 public function run(ctx: IActionContext): Bool {
        var game = ctx.getGame();
        var id: Int = ctx.getInt(Obfu.raw("id"));
        return game.root.GameManager.CONFIG.hasFamily(id);
    }
}