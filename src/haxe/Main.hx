import bugfix.Bugfix;
import debug.Debug;
import hf.Hf;
import merlin.Merlin;
import patchman.HostModuleLoader;
import patchman.IPatch;
import patchman.Patchman;

import quete.Quetes;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    bugfix: Bugfix,
    debug: Debug,
	quete : Quetes,
    merlin: Merlin,
    patches: Array<IPatch>,
    hf: Hf
  ) {
    Patchman.patchAll(patches, hf);
  }
}
